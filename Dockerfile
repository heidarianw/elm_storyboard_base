FROM node:latest as build

WORKDIR /my-project

# cache dependencies
COPY files/package-lock.json files/package.json ./
RUN npm install

# copy real source tree
# COPY files/postcss.config.js.production ./postcss.config.js
COPY files/elm.json files/tailwind.config.js files/style.css files/index.html ./
COPY files/src ./src
COPY files/postcss.config.js.production ./postcss.config.js

# install elm dependencies since parcel barfs otherwise
RUN npx elm make src/Main.elm --output=/dev/null

RUN npm run build
# set the startup command to run your binary
# CMD ["./# target/release/DOMAIN_NAME_api"]


FROM nginx:mainline-alpine
# RUN apt-get update && apt-get -y install ca-certificates libssl-dev && rm -rf /var/lib/apt/lists/*
# RUN sed -i '1idaemon off;' /etc/nginx/nginx.conf
EXPOSE 5000/tcp
COPY nginx/default.conf /etc/nginx/conf.d/

COPY --from=build /my-project/dist /var/www/html


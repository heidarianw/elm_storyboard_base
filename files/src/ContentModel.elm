module ContentModel exposing (init)

-- import Adapter.Content

import Adapter.Flags
import Html exposing (Html)
import Json.Decode exposing (decodeValue)
import Markdown
import Model exposing (..)
import Msg exposing (Msg)


init : Result String Adapter.Flags.Flags -> Result String ContentModel
init flagsResult =
    let
        rewriteOptions { content, options } =
            case options of
                "usa" ->
                    Ok <| ContentModel content

                _ ->
                    Err <| String.concat [ "Error in flags (unrecognized field value): {options: ", options, "}" ]
    in
    Result.andThen rewriteOptions flagsResult

module Route exposing (Route(..), routeParser, routeToString, routeToText, toRoute)

import Url
import Url.Builder
import Url.Parser exposing ((</>), (<?>), Parser, int, map, oneOf, parse, s, string, top)
import Url.Parser.Query


type alias Base64Email =
    String


type alias RedirectUrl =
    String


type Route
    = NotFound
    | Home
    | Scratch
    | Storyboard Int
    | StoryboardFull Int


toRoute : Url.Url -> Route
toRoute url =
    Maybe.withDefault NotFound <| parse routeParser url


routeToText : Route -> String
routeToText route =
    case route of
        NotFound ->
            "Not Found"

        Home ->
            "Home"

        Scratch ->
            "Scratch"

        Storyboard _ ->
            "Storyboard"

        StoryboardFull _ ->
            "Storyboard Full"


routeToString : Route -> String
routeToString route =
    case route of
        NotFound ->
            Url.Builder.absolute [] []

        Home ->
            Url.Builder.absolute [] []

        Scratch ->
            Url.Builder.absolute [ "scratch" ] []

        Storyboard index ->
            Url.Builder.absolute [ "storyboard", "cell", String.fromInt index ] []

        StoryboardFull index ->
            Url.Builder.absolute [ "storyboard", "full", String.fromInt index ] []


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ map Home top
        , map Scratch (s "scratch")
        , map Storyboard (s "storyboard" </> s "cell" </> Url.Parser.int)
        , map StoryboardFull (s "storyboard" </> s "full" </> Url.Parser.int)
        ]

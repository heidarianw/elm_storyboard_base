module Common exposing (boolToMaybe, dname, iffDict, iffHtml, iffInt, iffList, iffMaybe, iffSet, iffString, maybeToBool)

import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Set exposing (Set)


dname name =
    attribute "data-name" name


iffDict : Bool -> Dict a b -> Dict a b -> Dict a b
iffDict truth a b =
    if truth then
        a

    else
        b


iffHtml : Bool -> Html msg -> Html msg -> Html msg
iffHtml truth a b =
    if truth then
        a

    else
        b


iffList : Bool -> List a -> List a -> List a
iffList truth a b =
    if truth then
        a

    else
        b


iffInt : Bool -> Int -> Int -> Int
iffInt truth a b =
    if truth then
        a

    else
        b


iffMaybe : Bool -> Maybe a -> Maybe a -> Maybe a
iffMaybe truth a b =
    if truth then
        a

    else
        b


iffSet : Bool -> Set a -> Set a -> Set a
iffSet truth a b =
    if truth then
        a

    else
        b


iffString : Bool -> String -> String -> String
iffString truth a b =
    if truth then
        a

    else
        b


maybeToBool : Maybe a -> Bool
maybeToBool maybe =
    case maybe of
        Just _ ->
            True

        Nothing ->
            False


boolToMaybe : Bool -> a -> Maybe a
boolToMaybe bool a =
    if bool then
        Just a

    else
        Nothing

let options = 'usa';

let content = {
  version: 1,

  example_content: `
  <h1 class="font-semibold">hello world</h1>
  `,

  before_table: `
  <h1 class="font-semibold">Before</h1>
  `,

  after_table: `
  <h1 class="font-semibold">What if...</h1>
  `,
};

export {content, options};

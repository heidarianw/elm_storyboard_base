module Msg exposing (Msg(..))

import Browser
import Http
import Model exposing (..)
import RemoteData exposing (RemoteData)
import Url


type Msg
    = NoOp
    | BrowserLinkCell Int
    | BrowserLinkClicked Browser.UrlRequest
    | BrowserLinkClickedStr String
    | BrowserUrlChanged Url.Url
    | InputString InputStr String
    | StoryboardPortion Bool
    | Two Msg Msg
    | VersionSet Int

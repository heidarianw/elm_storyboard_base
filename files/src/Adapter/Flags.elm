module Adapter.Flags exposing (Flags, decode)

import Adapter.Content exposing (ContentData)
import Dict exposing (Dict)
import Json.Decode as Decode
import Json.Decode.Pipeline as Jpipe
import Model exposing (Model)


type alias Flags =
    { --version : Int
      content : ContentData
    , options : String
    , startingState : StartingState
    }


type alias StartingState =
    {}


decode : Decode.Decoder Flags
decode =
    Decode.succeed Flags
        |> Jpipe.required "content" Adapter.Content.decode
        |> Jpipe.required "options" Decode.string
        |> Jpipe.required "startingState" decodeLocalStorage


decodeLocalStorage : Decode.Decoder StartingState
decodeLocalStorage =
    Decode.succeed StartingState

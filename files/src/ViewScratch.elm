module ViewScratch exposing (view)

import Browser
import Common exposing (dname)
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Lazy exposing (..)
import Json.Decode
import MarkdownDeserialize exposing (asMarkdown)
import Model exposing (..)
import Msg exposing (Msg)
import Route
import Set exposing (Set)


view : ContentModel -> Model -> Html Msg
view contentModel model =
    asMarkdown [] """
<h2 class="font-semibold text-lg">Edit src/ViewScratch.elm to do whatever you want</h2>
<div class="h-4 w-4 bg-green-300"></div>

"""

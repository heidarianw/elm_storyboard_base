module Main exposing (init, main, subscriptions)

-- Copyright Greg Edwards, 2020. All rights reserved.

import Adapter.Flags
import Array exposing (Array)
import Browser
import Browser.Dom as Dom
import Browser.Navigation as Nav
import ContentModel
import Dict exposing (Dict)
import Json.Decode exposing (decodeValue)
import List
import List.Extra
import Model exposing (..)
import Msg exposing (Msg)
import Process
import Request
import Route
import Set exposing (Set)
import Task
import Update
import Url
import View


init : Json.Decode.Value -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flagsJson url key =
    let
        ( domain, domainOverride ) =
            Model.domainFor Nothing url

        routeActive =
            Route.toRoute url

        flagsResult =
            Result.mapError Json.Decode.errorToString <| decodeValue Adapter.Flags.decode flagsJson

        storyboardPortionArray =
            Array.fromList [ StoryboardPortion1, StoryboardPortion2, StoryboardPortion3, StoryboardPortion4, StoryboardPortion5 ]

        storyboardPortion =
            StoryboardPortion4
    in
    ( { nav =
            { key = key
            , domain = domain
            , domainOverride = domainOverride
            , routeActive = routeActive
            , url = url
            }
      , contentModelResult = ContentModel.init flagsResult
      , httpModel = { newHttpModel | initialDataLoaded = False }
      , storyboardPortion = storyboardPortion
      , storyboardPortionIndex = Maybe.withDefault 0 <| Maybe.map (\( i, _ ) -> i) <| List.Extra.find (\( i, x ) -> x == storyboardPortion) (Array.toIndexedList storyboardPortionArray)
      , storyboardPortionArray = storyboardPortionArray
      , version = 0
      }
      -- , Cmd.none
    , Update.jumpToElement "cells" "focusedCell"
    )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch []



-- MAIN


main : Program Json.Decode.Value Model Msg
main =
    Browser.application
        { init = init
        , view = View.view
        , update = Update.update
        , subscriptions = subscriptions
        , onUrlChange = Msg.BrowserUrlChanged
        , onUrlRequest = Msg.BrowserLinkClicked
        }

const {Elm} = require('./Main.elm');
const {content, options} = require('./content_english.js');

const key_name = 'DOMAIN_NAME-store-v2';
const startingState = {};

// if (module.hot) { module.hot.accept() } //Parcel development mode, do not reload the entire page

var app = Elm.Main.init({flags: {content, options, startingState}});

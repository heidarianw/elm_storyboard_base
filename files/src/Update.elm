module Update exposing (jumpToElement, update)

import Adapter.Flags
import Array exposing (Array)
import Browser
import Browser.Dom as Dom
import Browser.Navigation as Nav
import Common exposing (..)
import Dict exposing (Dict)
import Model exposing (Domain, HttpFrom(..), InputStr(..), Model, StoryboardPortion(..), domainFor, newHttpModel, urlToDomain)
import Msg exposing (..)
import Ports
import Process
import Regex
import RemoteData exposing (RemoteData(..))
import Request
import Rest
import Route
import Set exposing (Set)
import Task
import Url


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        httpModel =
            model.httpModel

        modelHttpTouched =
            { model | httpModel = newHttpModel }
    in
    case msg of
        NoOp ->
            ( model, Cmd.none )

        BrowserLinkCell index ->
            ( model, Nav.pushUrl model.nav.key <| Route.routeToString <| Route.Storyboard index )

        BrowserLinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.nav.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        BrowserLinkClickedStr urlString ->
            ( model, Nav.pushUrl model.nav.key urlString )

        BrowserUrlChanged url ->
            let
                nav =
                    model.nav

                ( domain, _ ) =
                    domainFor nav.domainOverride url
            in
            ( { model
                | nav =
                    { nav
                        | url = url
                        , routeActive = Route.toRoute url
                        , domain = domain
                    }
              }
            , Cmd.none
            )

        InputString inputStr str ->
            let
                newModel =
                    case inputStr of
                        InputStrNoOp ->
                            -- { model | inputExample = str }
                            model
            in
            ( newModel, Cmd.none )

        StoryboardPortion more ->
            let
                op =
                    if more then
                        (+)

                    else
                        (-)

                newIndex =
                    clamp 0 (Array.length model.storyboardPortionArray - 1) (op model.storyboardPortionIndex 1)
            in
            ( { model | storyboardPortionIndex = newIndex, storyboardPortion = Maybe.withDefault StoryboardPortion1 <| Array.get newIndex model.storyboardPortionArray }
            , jumpToElement2 "cells" "focusedCell"
            )

        Two msg1 msg2 ->
            update msg1 model
                |> Tuple.first
                |> update msg2

        VersionSet int ->
            ( { model | version = int }, Cmd.none )


remoteDataToString : RemoteData String a -> String
remoteDataToString remoteResponse =
    case remoteResponse of
        NotAsked ->
            ""

        Loading ->
            "Saving...."

        Failure err ->
            "Error, could not save"

        Success news ->
            "Saved"


jumpToBottom : String -> Cmd Msg
jumpToBottom id =
    Dom.getViewportOf (Debug.log "scroll this id" id)
        |> Task.andThen (\info -> Dom.setViewport 0 info.scene.height)
        |> Task.attempt (\e -> Msg.NoOp)


jumpToElement : String -> String -> Cmd Msg
jumpToElement id1 id2 =
    jumpToElementTask id1 id2
        |> Task.attempt (\e -> Msg.NoOp)


jumpToElement2 : String -> String -> Cmd Msg
jumpToElement2 id1 id2 =
    -- NOTE BUG in Elm? seemed like every other time Dom.getElement would return wrong element.y coordinates, so call it twice
    Task.sequence [ jumpToElementTask id1 id2, jumpToElementTask id1 id2 ]
        |> Task.map (\x -> Maybe.withDefault () <| List.head <| List.drop 1 x)
        |> Task.attempt (\e -> Msg.NoOp)


jumpToElementTask : String -> String -> Task.Task Dom.Error ()
jumpToElementTask id1 id2 =
    Dom.getElement id2
        |> Task.andThen
            (\info ->
                let
                    _ =
                        Debug.log "inof" info
                in
                if info.element.y == 0 then
                    Task.succeed ()

                else
                    Dom.setViewportOf id1 0 info.element.y
            )

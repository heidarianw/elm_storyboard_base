module LpathTransformTest exposing (suite)

import Dict exposing (Dict)
import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import LpathTransform exposing (..)
import Model exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "example"
        [ test "works" <|
            \_ ->
                let
                    input =
                        [ 1,2 3]

                    expected =
                        [ 10, 20, 30]
                in
                Expect.equal expected (List.map (*10) input)
        ]

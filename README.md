 ## Rapid Prototyping Workshop Instructions:
1. Fork the repository
2. Clone the repository
3. `cd elm_storyboard_base/files` 
4. `npm install`
5. `npm run dev`
6. Go to localhost:1236/scratch
7. Make a change to the HTML in `src/ViewScratch.elm` and save file
8. Go back to localhost:1236/scratch and see your changes! Hooray!
9. In the directory where you have installed ngrok, run `ngrok http 1236` and navigate to the link it provides.